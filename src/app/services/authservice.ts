import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import { Token } from '../model/token';

@Injectable()
export class AuthService {
  isLoggedin: boolean;

  constructor(private http: Http) {
      const usercreds = [
        {
            'username': 'adetunji',
            'password':  'oduyela'
        }
      ];
  }

  loginfn(usercreds) {
      this.isLoggedin = false;
      var headers = new Headers();
      var creds = 'name' + usercreds.username + '&password=' + usercreds.password;

      headers.append('Content-Type', 'application/X-www-form=urlencoded');
      return new Promise((resolve) => {
            // this.http.post('http:///localhost:4204/authenticate', creds, {
            //     headers: headers
            // }).subscribe((data) => {
            //     if(data.json().success) {
            //         window.localStorage.setItem('auth_key-homepage', data.json().token);
            //         this.isLoggedin = true;
            //         resolve(this.isLoggedin);
            //     }
            // })
            this.getAdminUsersAllow().subscribe((data) => {
                if(data) {
                    console.log(data);
                    window.localStorage.setItem('auth_key-homepage', 'adetunji.oduyela');
                    this.isLoggedin = true;
                    resolve(this.isLoggedin);
                }
            })
      });
  }

  extractData(res: Response){
      return res.json();
  }

  getAdminUsersAllow(): Observable<Token[]> {
    const mockData = [
        {
            "enterpriseId": "adetunji.oduyela"
        },
        {
            "enterpriseId": "adam.james"
        }
    ];

    const tokens = [];
    mockData.forEach(item => {
      const token = new Token();
      token.enterpriseId = item.enterpriseId;
      tokens.push(token);
    });
    return Observable.of(tokens);


  }

}
