import { Component } from '@angular/core';
import { AuthService } from './services/authservice';

@Component({
  selector: 'app-root',
  providers: [AuthService],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';
  

  ngOnInit() {
    window.sessionStorage.setItem('auth_key_homepage', 'adetunji.oduyela');
    console.log('sessionStorage auth_key_homepage is: ' + window.sessionStorage.getItem('auth_key_homepage'));
  }
}
